+function ($) {
  'use strict';

  var secondaryNav = function() {
  	var $link = $('.nav-sub a');

  	$link.on('click', function(e) {
  		e.preventDefault();

  		var $icon = $(this).find('.icon'),
  			$panel = $(this).parent('.nav-sub').find('.nav-panel');

  		//close all panels opened
  		if ($('.in').length) {
  			//set new icon to the open nav
  			$('.in').siblings().find('.icon').toggleClass('icon-rotate text-yellow-lightning');
  			//close all navs
  			$('.in').not('.nav-panel.in').toggleClass('in');
  		}

  		//add class to the icon
  		$icon.toggleClass('icon-rotate text-yellow-lightning');
  		
  		//open the panel by adding the class
  		$panel.toggleClass('in');
  		// $panel.animate({
  		// 	height: 'toggle'
  		// }, 300);

  	});
  }();
}(jQuery);